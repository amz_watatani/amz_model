<?php
namespace Amz\Model;

abstract class DataMapper
{
	private function __construct()
	{
	}

	/**
	 * データモデルクラスを連想配列にして返す
	 *
	 * @param	DataModel[]	$models
	 * @return	array
	 */
	protected static function toAssocArray($models)
	{
		$models = is_array($models) ? $models : array($models);
		$insert_data_arr = [];
		foreach ($models as $model)
		{
			$insert_data_arr[] = $model->toArray();
		}

		return $insert_data_arr;
	}

	/**
	 * 引数をint型にキャストする
	 *
	 * @param	mixed	$value
	 */
	protected static function castToInt(&$value)
	{
		if ($value === null)
		{
			return;
		}

		if (is_array($value))
		{
			foreach ($value as &$val)
			{
				$val = (int)$val;
			}
		}
		else
		{
			$value = (int)$value;
		}
	}

	/**
	 * 引数をstring型にキャストする
	 *
	 * @param	mixed	$value
	 */
	protected static function castToString(&$value)
	{
		if ($value === null)
		{
			return;
		}

		if (is_array($value))
		{
			foreach ($value as &$val)
			{
				$val = (string)$val;
			}
		}
		else
		{
			$value = (string)$value;
		}
	}
}
