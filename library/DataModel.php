<?php
namespace Amz\Model;

/**
 * データモデルクラス
 *
 * このクラスを継承して各テーブルクラスを作成する
 */
abstract class DataModel
{
	const
		BOOLEAN = 'boolean',
		INTEGER = 'integer',
		DOUBLE  = 'double',
		FLOAT   = 'double',
		STRING  = 'string',
		DATETIME = 'dateTime';

	/**
	 * @var array データ
	 */
	protected $_data = array();

	/**
	 * @var array 継承したクラスでスキーマ情報を定義する
	 */
	protected static $_schema = array();

	/**
	 * コンストラクタ
	 *
	 * @param	array	$properties
	 */
	function __construct(array $properties=array()) {
		if(!empty($properties)) {
			$this->fromArray($properties);
		}
	}

	/**
	 * 複製
	 */
	function __clone() {
		foreach($this->_data as $prop => $val) {
			if(is_object($val)) {
				$this->_data[$prop] = clone $this->_data[$prop];
			}
		}
	}

	/**
	 * プロパティの取得
	 *
	 * @param	string	$prop	プロパティ名
	 * @return	null
	 * @throws	\InvalidArgumentException
	 */
	function __get($prop) {
		if (isset($this->_data[$prop])) {
			return $this->_data[$prop];
		} elseif (isset(static::$_schema[$prop])) {
			return null;
		} else {
			throw new \InvalidArgumentException;
		}
	}

	/**
	 * isset() あるいは empty() をアクセス不能プロパティに対して実行したときに起動します。
	 *
	 * @param	string	$prop
	 * @return	bool
	 */
	function __isset($prop) {
		return isset($this->_data[$prop]);
	}

	/**
	 * プロパティの設定
	 *
	 * @param	string	$prop
	 * @param	mixed	$val
	 * @return bool|float|int|string|void
	 */
	function __set($prop, $val) {
		if (!isset(static::$_schema[$prop])) {
			throw new \InvalidArgumentException($prop.'はセットできません');
		}

		$schema = static::$_schema[$prop];
		$type = gettype($val);

		if ($schema === self::DATETIME) {
			if ($val instanceof \DateTime) {
				$this->_data[$prop] = $val;
			} else {
				$this->_data[$prop] = null;
				if (!is_null($val)) {
					$this->_data[$prop] = new \DateTime($val);
				}
			}
			return;
		}

		if ($type === $schema) {
			$this->_data[$prop] = $val;
			return;
		}

		if (is_null($val)) {
			$this->_data[$prop] = null;
			return;
		}

		switch ($schema) {
			case self::BOOLEAN:
				return $this->_data[$prop] = (bool)$val;
			case self::INTEGER:
				return $this->_data[$prop] = (int)$val;
			case self::DOUBLE:
				return $this->_data[$prop] = (double)$val;
			case self::STRING:
			default:
				return $this->_data[$prop] = (string)$val;
		}
	}

	/**
	 * データを配列で返す
	 *
	 * @return array
	 */
	function toArray() {
		$arr = array();
		foreach($this->_data as $prop => $val) {
			$arr[$prop] = is_object($val) ? clone $val : $val;
		}
		return $arr;
	}

	/**
	 * 配列からデータを設定する
	 *
	 * @param array $arr
	 * @return $this
	 */
	function fromArray(array $arr) {
		foreach ($arr as $key => $val) {
			$this->__set($key, is_object($val) ? clone $val : $val);
		}
		return $this;
	}

	/**
	 * DateTimeクラスを文字列(Y-m-d H:i:s)にコンバートする
	 */
	function convertDateTimeToString() {
		foreach ($this->_data as &$val) {
			if ($val instanceof \DateTime) {
				$val = $val->format('Y-m-d H:i:s');
			}
		}
	}

	/**
	 * データモデル配列からデータを配列にして返す
	 *
	 * @param	DataModel[]	$list
	 * @return	array
	 */
	static function toArrayList(array $list)
	{
		$result = array();
		foreach($list as $key => $model) {
			$result[$key] = $model->toArray();
		}
		return $result;
	}

	/**
	 *
	 * @param	array	$list
	 * @return	array
	 */
	static function fromArrayList(array $list)
	{
		$modelClass = get_called_class();

		$result = array();
		foreach($list as $key => $arr) {
			$result[$key] = new $modelClass($arr);
		}
		return $result;
	}

	/**
	 * @return array
	 */
	public static function getSchemaKeys()
	{
		return array_keys(static::$_schema);
	}

	abstract function isValid();
}
